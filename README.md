# Sphere Sensor Main Control Board 

This PCB mounts most of the sphere sensors and the microcontrollers to interface them. 
![Main Schematic](docs/Main_Control_Board-1.png)
![PCB](docs/IMG_0047.JPG)

## Assembly 


## Flashing 

### Atmega328p User Interface IC 

1. Disconnect the soldered board from the Odroid AND the Power distribution board. 
2. Connect the Arduino UNO to the PC and program the Arduino as ISP firmware through the Arduini IDE
3. Connect the Arduino to the control board as follows: 
	* PIN_RESET 10
	* PIN_MOSI	11
	* PIN_MISO	12
	* PIN_SCK	13
	* PIN_GND   GND
	* PIN_5V    5V 
4. Select the Arduino Uno from the boards list, arduino as ISP in the programmers option and click "Burn Bootloader"
5. If succesfull, disconnect the Uno and reconnect the Odroid and power distribution board. 
6. With the bootloader burned, the ODROID should now be able to program the user interface firmware. 

### LPC1768 

1. The LPC1768 bootloader is included in the chip from the factory. The firmware is flashed with the utility and script included in the LPC1768 programmer repository from the ODROID computer. It can also be programmed with a J-Link programmer through the JTAG header.


## Odroid Connector Pinout

| Odroid Pin Name | Header Pin Number | WiringPi Pin Number | Function |
|-----------------|-------------------|---------------------|----------|
| XE.INT 10       | 15 				  | 7 					| LPC1768 Reset | 
| XE.INT 11       | 18 				  | 4 					| 1.8V - 3.3V Level Shifter Output Enable | 
| XE.INT 13       | 13 				  | 2 					| LPC1768 Boot | 
| XE.INT 14       | 17 				  | 3 					| Serial MUX - 1  | 
| XE.INT 15       | 25 				  | 5 					| User Interface microcontroller reset | 
| XE.INT 21       | 21 				  | 26 					| Serial MUX - 2 | 
| XE.INT 22       | 19 				  | 22 					| Serial MUX - 3 | 
| XE.INT 25       | 27 				  | 27 					| NINAB1 Reset | 
| UART.RX 		  | 6 			      | 16 					| Serial RX    | 
| UART.TX 		  | 8 				  | 15     				| Serial TX    |
| PWRON 		  | 12 				  | - 					| 			   | 



## Testing procedure


1. Inspect pins of ICs for solder bridges 
2. Measure resistance between GND and +5V and +3.3V rails and check for possible shorts. 
3. Measure resistance between GND and SCL and SDA lines and check for possible shorts.  
3. Connect programmer to UI ICSP header and upload code to test LED strip. 
4. Program the LPC1768 

