EESchema Schematic File Version 4
LIBS:Main_Control_Board-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title "Main Sensors Board "
Date "2017-10-09"
Rev "0.3"
Comp "DROP Lab, University of Michigan"
Comment1 "Designed by Eduardo Iscar "
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5500 3500 5750 3500
Wire Wire Line
	5750 3300 5750 3500
Wire Wire Line
	5750 3600 5500 3600
Connection ~ 5750 3500
Wire Wire Line
	4900 3500 4900 3300
Wire Wire Line
	4900 3300 5750 3300
Wire Wire Line
	5950 3300 5950 3350
Connection ~ 5750 3300
$Comp
L Main_Control_Board-rescue:GND #PWR053
U 1 1 58147EE6
P 5950 3350
F 0 "#PWR053" H 5950 3100 50  0001 C CNN
F 1 "GND" H 5950 3200 50  0000 C CNN
F 2 "" H 5950 3350 50  0000 C CNN
F 3 "" H 5950 3350 50  0000 C CNN
	1    5950 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5500 3700 6250 3700
Wire Wire Line
	5500 3800 6250 3800
Text Label 6250 3700 2    60   ~ 0
PWM_FR
Text Label 6250 3800 2    60   ~ 0
PWM_UL
Text Label 5950 4100 2    60   ~ 0
PWM_UR
Text Label 5000 4100 2    60   ~ 0
PWM_FL
Wire Wire Line
	5000 3600 4600 3600
Wire Wire Line
	4500 3700 4550 3700
Wire Wire Line
	4550 3800 5000 3800
Text Label 4700 3600 0    60   ~ 0
+3.3V
Text Label 4700 3700 0    60   ~ 0
+5V
Text Label 4700 3800 0    60   ~ 0
+5V
Text Label 4800 3900 0    60   ~ 0
SDA1
Text Label 4800 4000 0    60   ~ 0
SCL1
Text Label 5850 3900 2    60   ~ 0
D_out-
Text Label 5850 4000 2    60   ~ 0
D_out+
Wire Wire Line
	4550 3700 4550 3800
Wire Wire Line
	4900 3500 5000 3500
Text HLabel 4600 3600 0    60   Input ~ 0
+3.3V
Text HLabel 4500 3700 0    60   Input ~ 0
+5V
Connection ~ 4550 3700
Text HLabel 4600 3900 0    60   Input ~ 0
SDA
Text HLabel 4600 4000 0    60   Input ~ 0
SCL
Text HLabel 6250 3700 2    60   Input ~ 0
PWM_FR
Text HLabel 6250 3800 2    60   Input ~ 0
PWM_UL
Text HLabel 6050 4100 2    60   Input ~ 0
PWM_UR
Text HLabel 4600 4100 0    60   Input ~ 0
PWM_FL
Text HLabel 6050 3900 2    60   Input ~ 0
D_out-
Text HLabel 6050 4000 2    60   Input ~ 0
D_out+
Text Notes 3950 2800 0    118  ~ 0
Power Management Board Connector
Wire Wire Line
	5750 3500 5750 3600
Wire Wire Line
	5750 3300 5950 3300
Wire Wire Line
	4550 3700 5000 3700
$Comp
L Connector_Generic:Conn_02x11_Odd_Even J?
U 1 1 5BC5CE8D
P 5200 4000
F 0 "J?" H 5250 4717 50  0000 C CNN
F 1 "Conn_02x11_Odd_Even" H 5250 4626 50  0000 C CNN
F 2 "" H 5200 4000 50  0001 C CNN
F 3 "~" H 5200 4000 50  0001 C CNN
	1    5200 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 3900 5000 3900
Wire Wire Line
	4600 4000 5000 4000
Wire Wire Line
	4600 4100 5000 4100
Wire Wire Line
	5500 4100 6050 4100
Wire Wire Line
	5500 3900 6050 3900
Wire Wire Line
	5500 4000 6050 4000
$EndSCHEMATC
