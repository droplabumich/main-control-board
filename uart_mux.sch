EESchema Schematic File Version 4
LIBS:Main_Control_Board-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title "Main Sensors Board "
Date "2017-10-09"
Rev "0.3"
Comp "DROP Lab, University of Michigan"
Comment1 "Designed by Eduardo Iscar "
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Main_Control_Board-rescue:74HC4052 U5
U 1 1 5756D01B
P 5700 3750
F 0 "U5" H 5600 5050 60  0000 C CNN
F 1 "74HC4052" H 5750 3800 60  0000 C CNN
F 2 "Sphere_lib_footprint:so16N" H 5700 3750 60  0001 C CNN
F 3 "" H 5700 3750 60  0000 C CNN
F 4 "" H 5700 3750 60  0001 C CNN "FarnellLink"
F 5 "" H 5700 3750 60  0001 C CNN "DigiKeyLink"
F 6 "771-74HC4052D-T" H 5700 3750 60  0001 C CNN "MouserLink"
F 7 "U-4 " H 5700 3750 60  0001 C CNN "InternalName"
F 8 "" H 5700 3750 60  0001 C CNN "FarnellLink"
F 9 "" H 5700 3750 60  0001 C CNN "DigiKeyLink"
F 10 "771-74HC4052D-T" H 5700 3750 60  0001 C CNN "MouserLink"
F 11 "U-4 " H 5700 3750 60  0001 C CNN "InternalName"
	1    5700 3750
	1    0    0    -1  
$EndComp
Text HLabel 4850 3200 0    60   Input ~ 0
OD_U0_RX
Text HLabel 4850 3100 0    60   Input ~ 0
OD_U0_TX
Text HLabel 6700 2600 2    60   Input ~ 0
LPC_U0_RX
Text HLabel 6700 2700 2    60   Input ~ 0
U2_RX
Text HLabel 6700 2800 2    60   Input ~ 0
U3_RX
Text HLabel 6700 3050 2    60   Input ~ 0
LPC_U0_TX
$Comp
L Main_Control_Board-rescue:+3.3V #PWR033
U 1 1 5756D281
P 5100 2450
AR Path="/5756D281" Ref="#PWR033"  Part="1" 
AR Path="/5756CFEA/5756D281" Ref="#PWR033"  Part="1" 
F 0 "#PWR033" H 5100 2300 50  0001 C CNN
F 1 "+3.3V" H 5100 2590 50  0000 C CNN
F 2 "" H 5100 2450 50  0000 C CNN
F 3 "" H 5100 2450 50  0000 C CNN
	1    5100 2450
	1    0    0    -1  
$EndComp
$Comp
L Main_Control_Board-rescue:GND #PWR034
U 1 1 5756D2A0
P 4950 2750
F 0 "#PWR034" H 4950 2500 50  0001 C CNN
F 1 "GND" H 4950 2600 50  0000 C CNN
F 2 "" H 4950 2750 50  0000 C CNN
F 3 "" H 4950 2750 50  0000 C CNN
	1    4950 2750
	1    0    0    -1  
$EndComp
$Comp
L Main_Control_Board-rescue:+3.3V #PWR035
U 1 1 5756D2D9
P 5100 4300
AR Path="/5756D2D9" Ref="#PWR035"  Part="1" 
AR Path="/5756CFEA/5756D2D9" Ref="#PWR035"  Part="1" 
F 0 "#PWR035" H 5100 4150 50  0001 C CNN
F 1 "+3.3V" H 5100 4440 50  0000 C CNN
F 2 "" H 5100 4300 50  0000 C CNN
F 3 "" H 5100 4300 50  0000 C CNN
	1    5100 4300
	1    0    0    -1  
$EndComp
$Comp
L Main_Control_Board-rescue:GND #PWR036
U 1 1 5756D2EA
P 4900 4550
F 0 "#PWR036" H 4900 4300 50  0001 C CNN
F 1 "GND" H 4900 4400 50  0000 C CNN
F 2 "" H 4900 4550 50  0000 C CNN
F 3 "" H 4900 4550 50  0000 C CNN
	1    4900 4550
	1    0    0    -1  
$EndComp
Text HLabel 6700 3150 2    60   Input ~ 0
U2_TX
Text HLabel 6700 3250 2    60   Input ~ 0
U3_TX
Text HLabel 4850 3400 0    60   Input ~ 0
IO_SWITCH_1
Text HLabel 4850 3500 0    60   Input ~ 0
IO_SWITCH_2
Text HLabel 6600 4500 2    60   Input ~ 0
OD_U2_TX
Text HLabel 6600 4950 2    60   Input ~ 0
OD_U2_RX
Text HLabel 6600 4400 2    60   Input ~ 0
LPC_U1_TX
Text HLabel 6600 4850 2    60   Input ~ 0
LPC_U1_RX
Text HLabel 4900 4900 0    60   Input ~ 0
OUT_RX
Text HLabel 4900 5000 0    60   Input ~ 0
OUT_TX
$Comp
L Main_Control_Board-rescue:GND #PWR037
U 1 1 5756DEA3
P 5150 5400
F 0 "#PWR037" H 5150 5150 50  0001 C CNN
F 1 "GND" H 5150 5250 50  0000 C CNN
F 2 "" H 5150 5400 50  0000 C CNN
F 3 "" H 5150 5400 50  0000 C CNN
	1    5150 5400
	1    0    0    -1  
$EndComp
Text HLabel 4900 5200 0    60   Input ~ 0
IO_SWITCH_3
Wire Wire Line
	5300 3100 4850 3100
Wire Wire Line
	5100 2450 5100 2600
Wire Wire Line
	5100 2600 5300 2600
Wire Wire Line
	4950 2750 4950 2700
Wire Wire Line
	4950 2700 5150 2700
Wire Wire Line
	5300 2800 5150 2800
Wire Wire Line
	5150 2700 5150 2800
Connection ~ 5150 2700
Wire Wire Line
	5100 4300 5100 4400
Wire Wire Line
	5100 4400 5300 4400
Wire Wire Line
	4900 4550 4900 4500
Wire Wire Line
	4900 4500 5200 4500
Wire Wire Line
	5300 4600 5200 4600
Wire Wire Line
	5200 4500 5200 4600
Connection ~ 5200 4500
Wire Wire Line
	6150 2600 6700 2600
Wire Wire Line
	6700 2700 6150 2700
Wire Wire Line
	6150 2800 6700 2800
Wire Wire Line
	6150 3050 6700 3050
Wire Wire Line
	6150 3150 6700 3150
Wire Wire Line
	6150 3250 6700 3250
Wire Wire Line
	4850 3400 5300 3400
Wire Wire Line
	4850 3500 5300 3500
Wire Wire Line
	4850 3200 5300 3200
Wire Wire Line
	5150 2950 5300 2950
Connection ~ 5150 2800
Wire Wire Line
	5200 4750 5300 4750
Connection ~ 5200 4600
Wire Wire Line
	6600 4400 6150 4400
Wire Wire Line
	6150 4500 6600 4500
Wire Wire Line
	6150 4950 6600 4950
Wire Wire Line
	4900 4900 5300 4900
Wire Wire Line
	5300 5000 4900 5000
Wire Wire Line
	6600 4850 6150 4850
Wire Wire Line
	5300 5300 5150 5300
Wire Wire Line
	5150 5300 5150 5400
Wire Wire Line
	4900 5200 5300 5200
Text Label 5250 3400 2    60   ~ 0
IO_1
Text Label 5250 3500 2    60   ~ 0
IO_2
Text Label 5250 5200 2    60   ~ 0
IO_3
NoConn ~ 6150 2900
NoConn ~ 6150 3350
NoConn ~ 6150 4600
NoConn ~ 6150 4700
NoConn ~ 6150 5050
NoConn ~ 6150 5150
$Comp
L Main_Control_Board-rescue:74HC4052 U6
U 1 1 5756D067
P 5700 5550
F 0 "U6" H 5600 6850 60  0000 C CNN
F 1 "74HC4052" H 5750 5600 60  0000 C CNN
F 2 "Sphere_lib_footprint:so16N" H 5700 5550 60  0001 C CNN
F 3 "" H 5700 5550 60  0000 C CNN
F 4 "" H 5700 5550 60  0001 C CNN "FarnellLink"
F 5 "" H 5700 5550 60  0001 C CNN "DigiKeyLink"
F 6 "771-74HC4052D-T" H 5700 5550 60  0001 C CNN "MouserLink"
F 7 "U-4 " H 5700 5550 60  0001 C CNN "InternalName"
F 8 "" H 5700 5550 60  0001 C CNN "FarnellLink"
F 9 "" H 5700 5550 60  0001 C CNN "DigiKeyLink"
F 10 "771-74HC4052D-T" H 5700 5550 60  0001 C CNN "MouserLink"
F 11 "U-4 " H 5700 5550 60  0001 C CNN "InternalName"
	1    5700 5550
	1    0    0    -1  
$EndComp
Text Notes 3700 2000 0    118  ~ 0
UART Multiplexer
Wire Wire Line
	5150 2700 5300 2700
Wire Wire Line
	5200 4500 5300 4500
Wire Wire Line
	5150 2800 5150 2950
Wire Wire Line
	5200 4600 5200 4750
$Comp
L Device:R_Small R12
U 1 1 5BFA4EDB
P 3000 3850
F 0 "R12" H 3059 3896 50  0000 L CNN
F 1 "4k" H 3059 3805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3000 3850 50  0001 C CNN
F 3 "~" H 3000 3850 50  0001 C CNN
	1    3000 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R13
U 1 1 5BFA4F62
P 3250 3850
F 0 "R13" H 3309 3896 50  0000 L CNN
F 1 "4k" H 3309 3805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3250 3850 50  0001 C CNN
F 3 "~" H 3250 3850 50  0001 C CNN
	1    3250 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small R14
U 1 1 5BFA4F84
P 3500 3850
F 0 "R14" H 3559 3896 50  0000 L CNN
F 1 "4k" H 3559 3805 50  0000 L CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3500 3850 50  0001 C CNN
F 3 "~" H 3500 3850 50  0001 C CNN
	1    3500 3850
	1    0    0    -1  
$EndComp
$Comp
L Main_Control_Board-rescue:GND #PWR0103
U 1 1 5BFA502F
P 3000 4050
F 0 "#PWR0103" H 3000 3800 50  0001 C CNN
F 1 "GND" H 3000 3900 50  0000 C CNN
F 2 "" H 3000 4050 50  0000 C CNN
F 3 "" H 3000 4050 50  0000 C CNN
	1    3000 4050
	1    0    0    -1  
$EndComp
Wire Wire Line
	3000 3950 3000 4000
Wire Wire Line
	3000 4000 3250 4000
Wire Wire Line
	3500 4000 3500 3950
Connection ~ 3000 4000
Wire Wire Line
	3000 4000 3000 4050
Wire Wire Line
	3250 3950 3250 4000
Connection ~ 3250 4000
Wire Wire Line
	3250 4000 3500 4000
Wire Wire Line
	3000 3750 3000 3650
Wire Wire Line
	3000 3650 2800 3650
Wire Wire Line
	3250 3750 3250 3550
Wire Wire Line
	3250 3550 2800 3550
Text Label 2800 3650 0    60   ~ 0
IO_1
Wire Wire Line
	3500 3750 3500 3450
Wire Wire Line
	3500 3450 2800 3450
Text Label 2800 3550 0    60   ~ 0
IO_2
Text Label 2800 3450 0    60   ~ 0
IO_3
$EndSCHEMATC
